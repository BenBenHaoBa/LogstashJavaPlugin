package com.sailing.logstashplugins.input;

import co.elastic.logstash.api.*;
import com.sailing.util.IPUtil;
import org.jruby.util.log.Logger;
import org.jruby.util.log.LoggerFactory;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.function.Consumer;

/**
 * @Author: BenBen
 * @CreateTime: 2019-09-03  14:01
 * @Description: logstash中自定义input插件ping_sailing
 */
@LogstashPlugin(name = "ping_sailing")
public class PingSailing implements Input {
    public static final PluginConfigSpec<String> IP_CONFIG =
            PluginConfigSpec.stringSetting("ip", null, false, true);

    public static final PluginConfigSpec<Long> SCHEDULE_CONFIG =
            PluginConfigSpec.numSetting("schedule", 5);

    public static BlockingQueue<Map> consumerQueue = new LinkedBlockingDeque(10);
    private static Logger logger = LoggerFactory.getLogger(PingSailing.class);

    private String id;

    private String ip;
    private Long schedule;

    private List<Map<String, Object>> logList = null;

    private final CountDownLatch done = new CountDownLatch(1);
    private volatile boolean stopped;

    // all plugins must provide a constructor that accepts id, Configuration, and Context
    public PingSailing(String id, Configuration config, Context context) {
        // constructors should validate configuration options
        this.id = id;
        ip = config.get(IP_CONFIG);
        schedule = config.get(SCHEDULE_CONFIG);
    }

    @Override
    public void start(Consumer<Map<String, Object>> consumer) {

        // The start method should push Map<String, Object> instances to the supplied QueueWriter
        // instance. Those will be converted to Event instances later in the Logstash ent
        // processing pipeline.
        //
        // Inputs that operate on unbounded streams of data or that poll indefinitely for new
        // events should loop indefinitely until they receive a stop request. Inputs that produce
        // a finite sequence of events should loop until that sequence is exhausted or until they
        // receive a stop request, whichever comes first.

        try {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            consumer.accept(consumerQueue.take());
                        } catch (Exception e) {
                            logger.error("The queue error: ", e);
                        }
                    }
                }
            };
            // while (!stopped) {
            myThread();
            runnable.run();
            // }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            stopped = true;
            done.countDown();
        }
    }

    public void myThread() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Timer timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            Map<String, Object> map = new HashMap<>();
                            map.put("ping", IPUtil.ping(ip, 5));
                            map.put("ip", ip);
                            consumerQueue.put(map);
                        } catch (Exception e) {
                            logger.error("Ping error: ", e);
                        }
                    }
                }, new Date(), schedule * 1000 * 60);
            }
        };
        runnable.run();
    }

    @Override
    public void stop() {
        stopped = true; // set flag to request cooperative stop of input
    }

    @Override
    public void awaitStop() throws InterruptedException {
        done.await(); // blocks until input has stopped
    }

    @Override
    public Collection<PluginConfigSpec<?>> configSchema() {
        // should return a list of all configuration options for this plugin
        return Arrays.asList(IP_CONFIG, SCHEDULE_CONFIG);
    }

    @Override
    public String getId() {
        return this.id;
    }
}
