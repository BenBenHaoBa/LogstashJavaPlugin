package com.sailing.util;

import org.jruby.util.log.Logger;
import org.jruby.util.log.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
 * @Author: BenBen
 * @CreateTime: 2019-07-26  16:38
 * @Description: IP工具
 */
public class IPUtil {
    private static Logger logger = LoggerFactory.getLogger(IPUtil.class);

    public static boolean ping(String ip, int count) {
        BufferedReader in = null;
        Runtime r = Runtime.getRuntime();  // 将要执行的ping命令,此命令是windows格式的命令
        String pingCommand = "ping -c " + count + " " + ip;
        try {   // 执行命令并获取输出
            System.out.println(pingCommand);
            Process p = r.exec(pingCommand);
            if (p == null) {
                return false;
            }
            in = new BufferedReader(new InputStreamReader(p.getInputStream()));   // 逐行检查输出,计算类似出现=23ms TTL=62字样的次数
            int connectedCount = 0;
            String line = null;
            while ((line = in.readLine()) != null) {
                logger.info(line);
                connectedCount += getCheckResult(line);
            }   // 如果出现类似=23ms TTL=62这样的字样,出现的次数=测试次数则返回真
            logger.info("ping次数:" + connectedCount);
            return connectedCount == count;
        } catch (Exception ex) {
            ex.printStackTrace();   // 出现异常则返回假
            return false;
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //若line含有=18ms TTL=16字样,说明已经ping通,返回1,否則返回0.
    private static int getCheckResult(String line) {  // System.out.println("控制台输出的结果为:"+line);
        Pattern pattern1 = Pattern.compile(".*ttl=.*", Pattern.CASE_INSENSITIVE);
        Pattern pattern2 = Pattern.compile(".*TTL=.*", Pattern.CASE_INSENSITIVE);
        while (pattern1.matcher(line).find() || pattern2.matcher(line).find()) {
            return 1;
        }
        return 0;
    }
}