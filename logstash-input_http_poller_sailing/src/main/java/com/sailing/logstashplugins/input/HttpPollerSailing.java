package com.sailing.logstashplugins.input;

import co.elastic.logstash.api.*;
import com.alibaba.fastjson.JSONObject;
import com.sailing.util.HttpClientUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.function.Consumer;

/**
 * @Author: BenBen
 * @CreateTime: 2019-09-03  14:01
 * @Description: logstash中自定义input插件http_poller_sailing
 */
@LogstashPlugin(name = "http_poller_sailing")
public class HttpPollerSailing implements Input {
    public static final PluginConfigSpec<String> URL_GET_TOKEN_CONFIG =
            PluginConfigSpec.stringSetting("url_getToken", null);

    public static final PluginConfigSpec<String> USER_NAME_CONFIG =
            PluginConfigSpec.stringSetting("username", null);

    public static final PluginConfigSpec<String> USER_PASSWORD_CONFIG =
            PluginConfigSpec.stringSetting("password", null);

    public static final PluginConfigSpec<String> USER_APPINFO_CONFIG =
            PluginConfigSpec.stringSetting("appinfo", null);

    public static final PluginConfigSpec<String> URL_GET_LOG_CONFIG =
            PluginConfigSpec.stringSetting("url_getLog", null, false, true);
    public static final PluginConfigSpec<String> Schedule_CONFIG =
            PluginConfigSpec.stringSetting("schedule", "5");
    public static final PluginConfigSpec<String> Logpath_CONFIG =
            PluginConfigSpec.stringSetting("logpath", "/opt/dpm/cla_ws/api_log");

    public static BlockingQueue<Map> consumerQueue = new LinkedBlockingDeque(10000);

    private String id;

    private String url_getToken;
    private String username;
    private String password;
    private String appinfo;
    private String url_getLog;
    private String schedule;
    private String logpath;

    private List<Map<String, Object>> logList = null;

    private final CountDownLatch done = new CountDownLatch(1);
    private volatile boolean stopped;

    // all plugins must provide a constructor that accepts id, Configuration, and Context
    public HttpPollerSailing(String id, Configuration config, Context context) {
        // constructors should validate configuration options
        this.id = id;
        url_getToken = config.get(URL_GET_TOKEN_CONFIG);
        username = config.get(USER_NAME_CONFIG);
        password = config.get(USER_PASSWORD_CONFIG);
        appinfo = config.get(USER_APPINFO_CONFIG);
        url_getLog = config.get(URL_GET_LOG_CONFIG);
        schedule = config.get(Schedule_CONFIG);
        logpath = config.get(Logpath_CONFIG);
    }

    @Override
    public void start(Consumer<Map<String, Object>> consumer) {

        // The start method should push Map<String, Object> instances to the supplied QueueWriter
        // instance. Those will be converted to Event instances later in the Logstash ent
        // processing pipeline.
        //
        // Inputs that operate on unbounded streams of data or that poll indefinitely for new
        // events should loop indefinitely until they receive a stop request. Inputs that produce
        // a finite sequence of events should loop until that sequence is exhausted or until they
        // receive a stop request, whichever comes first.

        try {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            consumer.accept(consumerQueue.take());
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            };
            // while (!stopped) {
            myThread();
            runnable.run();
            // }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            stopped = true;
            done.countDown();
        }
    }

    public void myThread() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Date date = new Date();
                int minute = 60 * 1000 * 60;
                if (!StringUtils.isBlank(schedule)) {
                    String[] split = schedule.trim().split(" ");
                    if (split.length == 1)
                        minute = Integer.valueOf(split[0]) * 1000 * 60;
                    if (split.length == 2) {
                        date = getDate(split[0]);
                        minute = Integer.valueOf(split[1]) * 1000 * 60;
                    }
                }
                Timer timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        String path = logpath + "/api_log-'" + String.format("%tm", new Date()) + "-" + String.format("%td", new Date()) + "'";
                        File file = new File(path);
                        List<Map<String, Object>> logList = null;
                        try {
                            if (file.exists())
                                System.out.println("日志文件已存在");
                            else {
                                System.out.println("日志文件不存在");
                                if (!file.getParentFile().exists())
                                    if (!file.getParentFile().mkdirs()) {
                                        System.out.println("创建文件夹！" + file.getParentFile());
                                    } else {
                                        throw new Exception("创建文件夹失败！");
                                    }
                                System.out.println("将获取日志--------------");
                                if (!StringUtils.isBlank(url_getToken))
                                    logList = getLog();
                                else
                                    logList = (List<Map<String, Object>>) JSONObject.parseObject(HttpClientUtil.doGet(url_getLog)).getInnerMap().get("data");

                                if (logList == null)
                                    throw new Exception("获取日志失败！");
                                System.out.println("logSize:" + logList.size());
                                if (!file.createNewFile())
                                    throw new Exception("创建日志文件异常！");
                                FileOutputStream out = new FileOutputStream(file, true);
                                System.out.println("文件已创建！");
                                for (Map map : logList) {
                                    out.write((JSONObject.toJSONString(map) + "\n").getBytes());
                                    consumerQueue.put(map);
                                }
                                logList.clear();
                                out.close();
                            }
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                            logList.clear();
                            if (file.exists())
                                file.delete();
                        }
                        System.out.println("文件路径：" + path);
                    }
                }, date, minute);
            }
        };
        runnable.run();
    }

    private Date getDate(String time) {
        // schedule = "0:0:0"
        String[] split = time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(split[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(split[1]));
        calendar.set(Calendar.SECOND, Integer.valueOf(split[2]));
        return calendar.getTime();
    }

    private List<Map<String, Object>> getLog() throws Exception {
        HashMap<String, Object> admin = new HashMap<>();
        admin.put("userName", username);
        admin.put("password", password);
        admin.put("appinfo", appinfo);

        JSONObject jsonObject = JSONObject.parseObject(HttpClientUtil.doPost(url_getToken, admin));
        Map<String, Object> innerMap = jsonObject.getInnerMap();
        Map<String, Object> map = JSONObject.parseObject(HttpClientUtil.doGet(url_getLog, innerMap)).getInnerMap();
        return (List<Map<String, Object>>) map.get("data");
    }

    @Override
    public void stop() {
        stopped = true; // set flag to request cooperative stop of input
    }

    @Override
    public void awaitStop() throws InterruptedException {
        done.await(); // blocks until input has stopped
    }

    @Override
    public Collection<PluginConfigSpec<?>> configSchema() {
        // should return a list of all configuration options for this plugin
        return Arrays.asList(URL_GET_TOKEN_CONFIG, USER_NAME_CONFIG, USER_PASSWORD_CONFIG, USER_APPINFO_CONFIG, URL_GET_LOG_CONFIG, Schedule_CONFIG, Logpath_CONFIG);
    }

    @Override
    public String getId() {
        return this.id;
    }
}
